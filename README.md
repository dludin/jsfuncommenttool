# README #

JsfUnCommentTool is an eclipse plugin that provides
a couple of comment/uncomment functions to Eclipse 
source editors, intended mainly for JSF Tags. 
Tested in myeclipse 2017 and oxygen2.

It has these functions:
					
	
	"Comment XML inline     CTRL+6"	turns   <h:body>   into <!--h:body-->
	"Comment XML surround   CTRL+5" turns   <h:body>   into <!--\n  <h:body>  \n--> 
	"Comment with ui remove CTRL+4" turns   <h:body>   into <ui:remove>  <h:body>  </ui:remove>
	
	"Uncomment              CTRL+8"	turns all of them back (hopefully)


(although in oxygen2 the keyboard shortcuts for these
functions do not work yet.)

The plugin can be installed with eclipse's "Install from Site.." from this update site:

	https://dl.bintray.com/dludin/JsfUnCommentTool/JsfUnCommentToolUpdatesite/