package simplepopupmenu.popup;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.eclipse.core.runtime.ILog;

import simplepopupmenu.Activator;

public abstract class AbstractCommenter implements Commenting{
	int startOfTag;
	int endOfTag;
	String selectedText;
	boolean containsTag;
	static String TAG_OPEN = "<";
	static String TAG_CLOSE = ">";
	static String TAG_COMMENT_OPEN = "<!--";
	static String TAG_COMMENT_CLOSE = "-->";
	static String TAG_UIREMOVE_OPEN = "<ui:remove>";
	static String TAG_UIREMOVE_CLOSE = "</ui:remove>";
	private static final Pattern LEADING = Pattern.compile("^\\s+");
	ILog log = Activator.getDefault().getLog();

	public AbstractCommenter() {
		super();
	}
		
	public String getLeadingSpaces(String str) {
	    Matcher m = LEADING.matcher(str);
	    if(m.find()){
	        return m.group(0);
	    }
	    return "";  
	}

}
