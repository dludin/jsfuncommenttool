package simplepopupmenu.popup;

import java.io.BufferedReader;
import java.io.StringReader;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.eclipse.core.runtime.IStatus;
import org.eclipse.core.runtime.Status;
import org.eclipse.jface.dialogs.MessageDialog;

import simplepopupmenu.Activator;

public class CommentWithRemove extends AbstractCommenter /*implements Commenting*/{
	
	public CommentWithRemove() {
		super();
	}

	public String parseString(String text) {
		
		//log.log(new Status(IStatus.INFO, Activator.PLUGIN_ID, "in|" + text + "|"));

		String leadingSpaces = getLeadingSpaces(text);
		text = new StringBuilder(text).insert(leadingSpaces.length(), TAG_UIREMOVE_OPEN  + "\n" + leadingSpaces).toString();
		int idxTagClose = text.lastIndexOf(TAG_CLOSE);
		text = new StringBuilder(text).insert(idxTagClose + 1, "\n" + leadingSpaces + TAG_UIREMOVE_CLOSE).toString();
		
		//log.log(new Status(IStatus.INFO, Activator.PLUGIN_ID, "out|" + text + "|"));

		return text;
	}
	
}
