package simplepopupmenu.popup;

import org.eclipse.core.runtime.IStatus;
import org.eclipse.core.runtime.Status;

import simplepopupmenu.Activator;

public class CommentWithXml extends AbstractCommenter{
	
	public CommentWithXml() {
		super();
	}

	public String parseString(String text) {
		
		//log.log(new Status(IStatus.INFO, Activator.PLUGIN_ID, "in|" + text + "|"));

		int idxTagOpen = text.indexOf(TAG_OPEN);
		int idxTagClose = text.lastIndexOf(TAG_CLOSE);
		if (idxTagOpen >= 0 && idxTagClose > 0) {
			text = new StringBuilder(text).replace(idxTagOpen, idxTagOpen + TAG_OPEN.length(), TAG_COMMENT_OPEN).toString();
			
			idxTagClose = text.lastIndexOf(TAG_CLOSE);
			text = new StringBuilder(text).replace(idxTagClose, idxTagClose + TAG_CLOSE.length(), TAG_COMMENT_CLOSE).toString();
		}
		
		//log.log(new Status(IStatus.INFO, Activator.PLUGIN_ID, "out|" + text + "|"));

		return text;
	}

	
}
