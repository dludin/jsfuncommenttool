package simplepopupmenu.popup;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.eclipse.core.runtime.IStatus;
import org.eclipse.core.runtime.Status;

import simplepopupmenu.Activator;

public class CommentWithXmlSurround extends AbstractCommenter{

	public CommentWithXmlSurround() {
		super();
	}


	public String parseString(String text) {
		
		//log.log(new Status(IStatus.INFO, Activator.PLUGIN_ID, "in|" + text + "|"));

		String leadingSpaces = getLeadingSpaces(text);
		text = new StringBuilder(text).insert(leadingSpaces.length(), TAG_COMMENT_OPEN + "\n" + leadingSpaces).toString();
		text = new StringBuilder(text).insert(text.length(), leadingSpaces + TAG_COMMENT_CLOSE + "\n").toString();
		
		//log.log(new Status(IStatus.INFO, Activator.PLUGIN_ID, "out|" + text + "|"));
		
		return text;
	}
	
}
