package simplepopupmenu.popup;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class UnComment extends AbstractCommenter {

	public UnComment() {
		super();
	}

	public String parseString(String text) {

		if (text.contains(TAG_COMMENT_OPEN) && text.contains(TAG_COMMENT_CLOSE)) {

			startOfTag = text.indexOf(TAG_COMMENT_OPEN);
			endOfTag = text.indexOf(TAG_COMMENT_CLOSE);

			Pattern p = Pattern.compile( "\\S" + TAG_COMMENT_CLOSE );
			Matcher m = p.matcher(text);
			if (m.find()){ // probably an inline comment
				text = text.replaceAll(TAG_COMMENT_OPEN, TAG_OPEN);
				text = text.replaceAll(TAG_COMMENT_CLOSE, TAG_CLOSE);
			}
			else {			// probably a surrounding comment
				Pattern p2 = Pattern.compile(TAG_COMMENT_OPEN + "\\n");
				Matcher m2 = p2.matcher(text);
				Pattern p3 = Pattern.compile(TAG_COMMENT_CLOSE + "\\n");
				Matcher m3 = p3.matcher(text);
				if (m2.find() && m3.find()) {  
					text = text.replaceAll(TAG_COMMENT_OPEN + "\n", "");
					text = text.replaceAll(TAG_COMMENT_CLOSE + "\n", "");					
				}
				else {
					text = text.replaceAll(TAG_COMMENT_OPEN, "");
					text = text.replaceAll(TAG_COMMENT_CLOSE, "");
				}
			}
		} else if (text.contains(TAG_UIREMOVE_OPEN) && text.contains(TAG_UIREMOVE_CLOSE)) {
			text = text.replaceAll(TAG_UIREMOVE_OPEN, "");
			text = text.replaceAll(TAG_UIREMOVE_CLOSE, "");
		}

		return text;
	}
}
