package simplepopupmenu.popup.actions;

import org.eclipse.core.runtime.ILog;
import org.eclipse.jface.action.IAction;
import org.eclipse.jface.viewers.ISelection;
import org.eclipse.swt.widgets.Shell;
import org.eclipse.ui.IObjectActionDelegate;
import org.eclipse.ui.IWorkbenchPart;

import simplepopupmenu.Activator;
import simplepopupmenu.service.CommentService;

public class CommentWithRemoveAction implements IObjectActionDelegate {
	private Shell shell;
	ILog log = Activator.getDefault().getLog();

	public CommentWithRemoveAction() {
		super();
	}

	@Override
	public void setActivePart(IAction action, IWorkbenchPart targetPart) {
		shell = targetPart.getSite().getShell();
	}

	@Override
	public void run(IAction action) {
		new CommentService().commentWithRemoveTag();
	}

	@Override
	public void selectionChanged(IAction action, ISelection selection) {
	}
}