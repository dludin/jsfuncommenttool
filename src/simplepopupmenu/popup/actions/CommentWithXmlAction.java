package simplepopupmenu.popup.actions;

import org.eclipse.core.runtime.ILog;
import org.eclipse.jface.action.IAction;
import org.eclipse.jface.viewers.ISelection;
import org.eclipse.swt.widgets.Shell;
import org.eclipse.ui.IObjectActionDelegate;
import org.eclipse.ui.IWorkbenchPart;

import simplepopupmenu.Activator;
import simplepopupmenu.service.CommentService;

public class CommentWithXmlAction implements IObjectActionDelegate {
	private Shell shell;
	ILog log = Activator.getDefault().getLog();

	public CommentWithXmlAction() {
		super();
	}

	@Override
	public void setActivePart(IAction action, IWorkbenchPart targetPart) {
		shell = targetPart.getSite().getShell();
	}

	@Override
	public void run(IAction action) {
		new CommentService().commentWithXml();;

	}

	@Override
	public void selectionChanged(IAction action, ISelection selection) {
	}
}