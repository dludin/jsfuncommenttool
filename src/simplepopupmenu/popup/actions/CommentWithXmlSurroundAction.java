package simplepopupmenu.popup.actions;

import org.eclipse.core.runtime.ILog;
import org.eclipse.jface.action.IAction;
import org.eclipse.jface.viewers.ISelection;
import org.eclipse.swt.widgets.Shell;
import org.eclipse.ui.IObjectActionDelegate;
import org.eclipse.ui.IWorkbenchPart;
import simplepopupmenu.Activator;
import simplepopupmenu.service.CommentService;

public class CommentWithXmlSurroundAction implements IObjectActionDelegate {
	private Shell shell;
	ILog log = Activator.getDefault().getLog();

	public CommentWithXmlSurroundAction() {
		super();
	}

	@Override
	public void setActivePart(IAction action, IWorkbenchPart targetPart) {
		shell = targetPart.getSite().getShell();
	}

	@Override
	public void run(IAction action) {
		new CommentService().commentWithXmlSurround();

	}

	@Override
	public void selectionChanged(IAction action, ISelection selection) {
	}
}