package simplepopupmenu.service;

import org.eclipse.core.runtime.ILog;
import org.eclipse.core.runtime.IStatus;
import org.eclipse.core.runtime.Status;
import org.eclipse.jface.text.IDocument;
import org.eclipse.jface.text.ITextSelection;
import org.eclipse.jface.viewers.ISelection;
import org.eclipse.jface.viewers.ISelectionProvider;
import org.eclipse.ui.IEditorPart;
import org.eclipse.ui.IEditorSite;
import org.eclipse.ui.part.MultiPageEditorPart;
import org.eclipse.ui.texteditor.IDocumentProvider;
import org.eclipse.ui.texteditor.ITextEditor;
import org.eclipse.wst.xml.ui.internal.tabletree.XMLMultiPageEditorPart;
import simplepopupmenu.Activator;
import simplepopupmenu.popup.CommentWithRemove;
import simplepopupmenu.popup.CommentWithXml;
import simplepopupmenu.popup.CommentWithXmlSurround;
import simplepopupmenu.popup.Commenting;
import simplepopupmenu.popup.UnComment;

public class CommentService {
	ILog log = Activator.getDefault().getLog();

	public void commentWithRemoveTag(){
		CommentWithRemove xmlsel = new CommentWithRemove();
		commentGeneric(xmlsel);
	}
	public void commentWithXml(){
		CommentWithXml xmlsel = new CommentWithXml();
		commentGeneric(xmlsel);

	}
	public void commentWithXmlSurround(){
		CommentWithXmlSurround xmlsel = new CommentWithXmlSurround();
		commentGeneric(xmlsel);

	}
	public void uncomment() {
		UnComment xmlsel = new UnComment();
		commentGeneric(xmlsel);
	}


	private void commentGeneric(Commenting xmlsel) {
		try {
			// get editor
			IEditorPart editorPart = Activator.getDefault().getWorkbench().getActiveWorkbenchWindow().getActivePage()
					.getActiveEditor();
			int offset = 0;
			int length = 0;
			String selectedText = null;
			IEditorSite iEditorSite = editorPart.getEditorSite();
			if (iEditorSite != null) {
				// get selection provider
				ISelectionProvider selectionProvider = iEditorSite.getSelectionProvider();
				if (selectionProvider != null) {
					ISelection iSelection = selectionProvider.getSelection();
					// offset
					offset = ((ITextSelection) iSelection).getOffset();
					offset = ((ITextSelection) iSelection).getOffset();
					if (!iSelection.isEmpty()) {
						selectedText = ((ITextSelection) iSelection).getText();

						// length
						length = ((ITextSelection) iSelection).getLength();
						//log.log(new Status(IStatus.INFO, Activator.PLUGIN_ID, "offset: " + offset + "length: " + length));
						

						ITextEditor editor = null;
						IDocumentProvider dp = null;

						if (editorPart instanceof ITextEditor) {
							editor = (ITextEditor) editorPart;
						} else if (editorPart instanceof MultiPageEditorPart) {
							Object page = ((MultiPageEditorPart) editorPart).getSelectedPage();
							if (page instanceof ITextEditor) {
								editor = (ITextEditor) page;
							}
						} else if (editorPart instanceof org.eclipse.m2e.editor.pom.MavenPomEditor) {
							Object page = ((MultiPageEditorPart) editorPart).getSelectedPage();
							if (page instanceof ITextEditor) {
								editor = (ITextEditor) page;
							}
						}
						else if (editorPart instanceof XMLMultiPageEditorPart) {
							Object page = ((MultiPageEditorPart) editorPart).getSelectedPage();
							if (page instanceof ITextEditor) {
								editor = (ITextEditor) page;
							}
						}
					
						

						if (editor != null) {
							dp = editor.getDocumentProvider();
						}

						dp = editor.getDocumentProvider();
						IDocument doc = dp.getDocument(editor.getEditorInput());
						int offset2 = doc.getLineOffset(doc.getNumberOfLines() - 4);

						//log.log(new Status(IStatus.INFO, Activator.PLUGIN_ID, "offset2: " + offset2));

						doc.replace(offset, length, xmlsel.parseString(selectedText));
					}

				}
			}
		} catch (Exception e) {
			log.log(new Status(IStatus.ERROR, e.getMessage(), e.getLocalizedMessage()));

		}

	}
	

}
